package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();


		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);
		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();

		if (!isValid(user, errorMessages, request)) {
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages,HttpServletRequest request) {

		String name = user.getName();
		String account = user.getAccount();
		String password = request.getParameter("password");
		String confirmpassword = request.getParameter("password");
		int branch = user.getBranchId();
		int department = user.getDepartmentId();


		User user1 =  new UserService().select(user.getAccount());

		if (!(((branch == 1) && ((department == 1) || (department == 2))) || (((branch != 1) && ((department == 3) || (department == 4))))))  {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}

		if  (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if(!account.matches("^[a-zA-Z0-9]+$")){
			errorMessages.add("アカウント名は半角英数字とします");
		} else if(20 < account.length()){
			errorMessages.add("アカウント名は20文字以下とします");
		} else if(6 > account.length()){
			errorMessages.add("アカウント名は6文字以上とします");
		}

		if(user1 != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if (StringUtils.isEmpty(name)) {
			errorMessages.add("ユーザー名を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if(!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")){
			errorMessages.add("パスワードは記号を含む全ての半角文字とします");
		} else if(20 < password.length()){
			errorMessages.add("パスワードは20文字以下とします");
		} else if(6 >  password.length()){
			errorMessages.add("パスワードは6文字以上とします");
		}
		if(!password.equals(confirmpassword)) {
			errorMessages.add("パスワードと確認用パスワードが異なります");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}