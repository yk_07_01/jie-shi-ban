package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {




		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();
		User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));
		List<String> errorMessages = new ArrayList<String>();

		HttpSession session = request.getSession();

		String userId = request.getParameter("userId");

		if(StringUtils.isBlank(userId)) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}
		if (!userId.matches("^[0-9]+$")){
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}
		if(new UserService().select(Integer.parseInt(request.getParameter("userId"))) == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		request.setAttribute("user", user);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();
		List<String> errorMessages = new ArrayList<String>();


		User user = getUser(request);

		if (isValid(user, errorMessages, request)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user);

		session.setAttribute("loginUser", user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, List<String> errorMessages,HttpServletRequest request) {

		String name = user.getName();
		String account = user.getAccount();
		String password = request.getParameter("password");
		String confirmpassword = request.getParameter("password");

		User Loginuser =  new UserService().select(user.getAccount());

		int branch = user.getBranchId();
		int department = user.getDepartmentId();

		if ( (branch == 1) && (department == 2) || (branch == 1) && (department == 3) || (branch == 1) && (department == 4) || (branch == 2) && (department == 1) || (branch == 3) && (department == 1) || (branch == 4) && (department == 1) ) {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}


		if (StringUtils.isEmpty(name)) {
			errorMessages.add("ユーザー名を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if  (!StringUtils.isEmpty(account)) {
			if(!account.matches("^[a-zA-Z0-9]+$")){
				errorMessages.add("アカウント名は半角英数字とします");
			} else if(20 < account.length()){
				errorMessages.add("アカウント名は20文字以下とします");
			} else if(6 > account.length()){
				errorMessages.add("アカウント名は6文字以上とします");
			} else	if(Loginuser != null) {
				errorMessages.add("アカウントが重複しています");
			}
		}
		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if(!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")){
			errorMessages.add("パスワードは記号を含む全ての半角文字とします");
		} else if(20 < password.length()){
			errorMessages.add("パスワードは20文字以下とします");
		} else if(6 >  password.length()){
			errorMessages.add("パスワードは6文字以上とします");
		}

		if(!password.equals(confirmpassword)) {
			errorMessages.add("パスワードと確認用パスワードが異なります");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
