package beans;
import java.io.Serializable;

public class UserBranchDepartment implements Serializable {

	private int id;
	private String account;
	private String password;
	private String name;
	private String branch;
	private String department;
	private byte isStopped;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	public byte getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(byte isStopped) {
		this.isStopped = isStopped;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}