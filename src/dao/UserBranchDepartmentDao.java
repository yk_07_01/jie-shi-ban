package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("	SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    departments.name as department_name, ");
			sql.append("    users.is_stopped as is_stopped, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date DESC " );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> users = toUserBranchDepartment(rs);
			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setDepartment(rs.getString("department_name"));
				user.setBranch(rs.getString("branch_name"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setName(rs.getString("name"));

				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}
	}
}