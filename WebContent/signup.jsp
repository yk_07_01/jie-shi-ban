<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="signup" method="post">
			<br /><a href="management">ユーザー管理</a><br />
			<label for="account">アカウント名</label>
			<input name="account" id="account" value="${user.account}"/><br /> <br />
			<label for="password">パスワード</label>
			<input name="password" type="password"id="password" /> <br /> <br />
			<label for="password">確認用パスワード</label>
			<input name="confirmpassword" type="password" id="confirmpassword" /> <br /> <br />
			<label for="name">ユーザー名</label>
			<input name="name" id="name" value="${user.name}" /><br />
			<br />
			<p>
			支社
			<select name="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${user.branchId == branch.id}">
							<option value="${branch.id}"selected>${branch.name}</option>
						</c:if>
							<c:if test="${user.branchId != branch.id}">
								<option value="${branch.id}">${branch.name}</option>
							</c:if>
					</c:forEach>
			</select><br /> <br />
			</p>
			<p>
			部署
			<select name="departmentId">
					<c:forEach items="${departments}" var="department">
						<c:if test="${user.departmentId == department.id}">
							<option value="${department.id}"selected>${department.name}</option>
						</c:if>
							<c:if test="${user.departmentId != department.id}">
								<option value="${department.id}">${department.name}</option>
							</c:if>
					</c:forEach>
			</select>
			</p>
			<br /> <br /> <input type="submit" value="登録" /> <br />
		</form>
		<div class="copyright">Copyright(c)Yoshida kai</div>
	</div>
</body>
</html>