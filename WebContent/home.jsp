<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
</head>
	<script type="text/javascript">
		function disp() {

			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
			if (window.confirm('削除しますか')) {

				location.href = "./"; // example_confirm.html へジャンプ
				return true;

			}
			// 「OK」時の処理終了

			// 「キャンセル」時の処理開始
			else {

				window.alert('キャンセルしました'); // 警告ダイアログを表示
				return false;

			}
			// 「キャンセル」時の処理終了
		}
	</script>
<body>

	<div class="header">
		<a href="message">新規投稿</a> &emsp;
			<c:if test="${branchId == 1 && departmentId == 1}">
				<a href="management">ユーザー管理</a>&emsp;
			</c:if>
				<a href="logout">ログアウト</a>
	</div>

	<form action="./">
		<c:if test="${ not empty errorMessages }">
			<div class="e/rrorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
				<c:remove var="errorMessages" scope="session" />
		</c:if>
	<br />

		<div class="serch">
			<label for="day">日付</label>
			<input type = "date" id = "start" name = "start" value ="${start}"/>～
			<input type = "date" id = "end" name = "end"value ="${end}"/><br/>

			<label for="category">カテゴリ</label>
			<input  id = "category" name = "category"value ="${category}"/>
			<input type="submit" value="絞込み" />
		</div><br/>
	</form>

	<div class="messages">
		<c:forEach items="${messages}" var="message">
				<div class="message">
					ユーザー名：<span class="name"><c:out value="${message.name}" /></span><br />
					件名：<span class="title"><c:out value="${message.title}" /></span><br />
					カテゴリ：<span class="category"><c:out value="${message.category}" /></span><br />
					投稿内容：<span class="text"><pre><c:out value="${message.text}" /></pre></span>

			<div class="date">
				投稿日時：<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/ddHH:mm:ss" />
			</div>
				<br />

				<div class="deletemessage">
					<form action="deletemessage" method="post">
						<input type="hidden" name="messageId" value="${message.id}">
							<c:if test="${loginUser.id == message.userId}">
								<input type="submit" value="投稿削除" onclick="return disp()" />
							</c:if>
					</form>
				</div>

			</div>
			<div class="comments">
				<c:forEach items="${comments}" var="comment">
					<c:if test = "${comment.messageId == message.id }">
						ユーザー名：<span class="name"><c:out value="${comment.name}" /></span><br />
						コメント内容：<span class="text"><pre><c:out value="${comment.text}" /></pre></span><br />

						<div class="date">
							コメント日時：<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/ddHH:mm:ss" />
						</div>

							<div class="deletecomment">
								<form action="deletecomment" method="post">
									<input type="hidden" name="commentId" value="${comment.id}">
										<c:if test="${loginUser.id == comment.userId}">
											<input type="submit" value="コメント削除" onclick="return disp()" />
										</c:if>
								</form>
							</div>
					</c:if>
				</c:forEach>
			</div>

			<div class="comment">
				<form action="comment" method="post">
					<br /> コメント入力欄:<br />
					<textarea name="text" cols="20" rows="5" class="textbox"></textarea><br />
					<input type="hidden" name="messageId" value="${message.id}">
					<input type="submit" value="コメント投稿" /> <br />
				</form>
			</div>
		</c:forEach>
	</div>
		<br /> <br /> <br />
		<div class="copyright">Copyright(c)Yoshida kai</div>
</body>
</html>
