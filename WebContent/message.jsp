<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="message" method="post">
			<br />
			<a href="./">ホーム</a><br/>
				件名:<input type = "text" name="title" size="40" maxlength = "40" value="${title}"/><br />
				カテゴリ:<input type = "text" name="category" size = "40" maxlength = "20"value="${category}" /><br />
				投稿内容:<textarea name="text" cols = "40" rows = "10" class = "textbox">${text}</textarea><br /><br /><br /><br />
				<input type="submit" value="投稿" /> <br />
		</form>
		<div class="copyright">Copyright(c)Yoshida kai</div>
	</div>
</body>
</html>