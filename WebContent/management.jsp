<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		function disp() {

			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
			if (window.confirm('変更しますか')) {

				location.href = "management"; // example_confirm.html へジャンプ
				return true;

			}
			// 「OK」時の処理終了

			// 「キャンセル」時の処理開始
			else {

				window.alert('キャンセルしました'); // 警告ダイアログを表示
				return false;

			}
			// 「キャンセル」時の処理終了
		}
	</script>

<body>
	<div class="header">
		<a href="./">ホーム</a> &emsp; <a href="signup">ユーザー新規登録</a>
	</div>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<br />

		<div class="managements">

			<c:forEach items="${users}" var="user">
				<div class="management">
	アカウント：<span class="account"><c:out value="${user.account}" /></span><br />
			名前：<span class="name"><c:out value="${user.name}" /></span><br />
			支社：<span class="branch_id"><c:out value="${user.branch}" /></span><br />
			部署：<span class="department_id"><c:out value="${user.department}" /></span><br />
アカウント停止状態：<span class="istopped"><c:out value="${user.isStopped}" /></span><br/>
				</div>
					<form action="setting" method="get">
						<input type="hidden" name="userId" value="${user.id}">
						<input type="submit" value="編集" />
					</form>
						<form action="stop" method="post">
							<c:if test = "${loginUser != user.id }">
								<c:if test = "${user.isStopped == 1 }">
									<input type="hidden" name="isStopped" value="0">
									<input type="hidden" name="userId" value="${user.id}">
									<input type="submit" value="復活" onclick="return disp()"/>[停止中]
								</c:if>
									<c:if test = "${user.isStopped == 0 }">
										<input type="hidden" name="isStopped" value="1">
										<input type="hidden" name="userId" value="${user.id}">
										<input type="submit" value="停止" onclick="return disp()"/>[復活中]
									</c:if>
							</c:if>
						</form>
			</c:forEach><br/>
		</div>
	</div>
	<br />
	<br />
	<div class="copyright">Copyright(c)Yoshida kai</div>
</body>
</html>
